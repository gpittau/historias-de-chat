const fs = require('fs')
const path = require('path')

const indexHTML = fs
  .readFileSync(path.resolve(__dirname, '../ssr/index.html'))
  .toString()

const keyToRe = key => (new RegExp(`__${key.toUpperCase()}__`, 'g'))

const getIndexHTML = obj => Object.keys(obj)
  .reduce(
    (result, key) => result.replace(keyToRe(key),obj[key]),
    indexHTML
  )

module.exports = {
  getIndexHTML
}
