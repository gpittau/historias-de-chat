const { getIndexHTML } = require('../utils/getIndexHTML')
const functions = require('firebase-functions')
const admin = require('firebase-admin')

exports = module.exports = functions.https.onRequest(async ({url, path=""}, res) => {
	const [,what, action, uid=false] = path.split('/');

	if (what === 'stories') {
    try {
      if (uid) {
        const snapshot = await admin.database()
          .ref(`/stories/${uid}`)
          .once('value');
        const story = snapshot.val();
        res.status(200).send(getIndexHTML({
          url,
          title: `HDC: ${story.title}`,
          description: `${story.title}, (${story.author})`,
          author: story.author,
          logo_url: '/hdc.svg'
        }));
        console.log('onRequest Story:', story)
      } else {
        const storiesSnapshot = await admin.database()
          .ref(`stories`)
          .once('value');
        const stories = storiesSnapshot.val();
        res.status(200).send(getIndexHTML({
          url,
          title: `HDC: ${stories.length} Historias`,
          description: 'Historias de Chat',
          logo_url: '/hdc.svg'
        }));
        console.log('onRequest Stories:', stories.length)
      }
    } catch(err) {
      res.status(200).send(getIndexHTML({
        url,
        title: 'Historias de Chat',
        description: 'Historias contadas en Chat',
        logo_url: '/hdc.svg'
      }));
      //res.status(404).send('Not Found');
    }
  } else {
    res.status(200).send(getIndexHTML({
      url,
      title: 'Historias de Chat',
      description: 'Historias contadas en Chat',
      logo_url: '/hdc.svg'
    }));
  }
});

