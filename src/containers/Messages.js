import Message from './Message'
import Comment from './Comment'
import PropTypes from 'prop-types'
import React, { useEffect, useState } from 'react'
import Scrollbar from 'rmw-shell/lib/components/Scrollbar/Scrollbar'
import { withTheme } from '@material-ui/core/styles'

const normalizedEqual = (one, two) => one.normalize('NFD').replace(/[\u0300-\u036f]/g, '') === two.normalize('NFD').replace(/[\u0300-\u036f]/g, '')
const Messages = props => {
  const [msgList, setMsgList] = useState([])
  let listEnd = null
  const initMessages = ({conversation = ''}) => {
    const messages = conversation
      .split(/\n/g)
      .map(line => line
        .split(/^\s*(?:\(([^)]+)\))?\s*(?:([^:]+):)?\s?(.*)/g)
      )
      .map(([_, when, who, what]) => ({when, who, what}))

    const endMessage = {what: 'FIN'}
    setMsgList([...messages, endMessage])
  }

  useEffect(() => { initMessages(props) },[props])
  useEffect(() => {
    if (listEnd) {
      listEnd.scrollIntoView({ behavior: 'smooth' })
    }
  },[listEnd, msgList])

  const { currentMessage, theme } = props

  const renderList = (messages) => {
    if (messages === undefined || messages.length === 0) {
      return <div />
    }

    let first
    return messages.slice(0, currentMessage).map(({who, when, what}, i) => {
      if(!first) {
        first = who
      }

      if (!who) {
        return (
          <Comment
            key={i}
            what={what}
            when={when}
          />
        )
      }

      const firstPerson = normalizedEqual(first, who)
      const backgroundColor = theme.palette[firstPerson ? 'primary' : 'secondary'].light
      const color = theme.palette.getContrastText(backgroundColor)
      return (
        <Message
          key={i}
          line={{who, when, what}}
          firstPerson={firstPerson}
          backgroundColor={backgroundColor}
          color={color}
        />
      )
    })
  }

  return (
    <Scrollbar
      style={{
        backgroundColor: theme.palette.background.default,
        width: '100%'
      }}
      renderView={props => <div {...props} style={{ ...props.style, overflowX: 'hidden' }} />}
    >
      <div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
        <div style={{ maxWidth: 600, margin: 8, width: '100%' }}>
          {renderList(msgList)}
        </div>
      </div>
      <div
        style={{ float: 'left', clear: 'both' }}
        ref={el => {
          listEnd = el
        }}
      />
    </Scrollbar>
  )
}

Messages.propTypes = {
  theme: PropTypes.object.isRequired,
  style: PropTypes.object.isRequired,
  conversation: PropTypes.string.isRequired,
  currentMessage: PropTypes.number.isRequired
}

export default withTheme(Messages)
