import PropTypes from 'prop-types'
import React from 'react'
import moment from 'moment'
import { compose } from 'redux'
import { injectIntl } from 'react-intl'
import { withTheme } from '@material-ui/core/styles'

const Comment = props => {

  const {
    theme,
    when,
    what,
    intl
  } = props

  const backgroundColor = theme.palette.background.paper
  const color = theme.palette.getContrastText(backgroundColor)

  return (
    <div style={{ width: '100%' }}>
      <div>
        <div
          style={{
            display: 'flex',
            width: '100%',
            justifyContent: 'center'
          }}
        >
          <div
            style={{
              ...theme.chip,
              margin: 1,
              marginTop: 8,
              boxShadow: theme.shadows[3],
              borderRadius: '8px 8px 8px 8px',
              backgroundColor: backgroundColor,
              color: color,
              fontFamily: theme.typography.fontFamily
            }}
          >
            <div
              style={{
                display: 'flex',
                margin: 5,
                padding: 0,
                flexOrientation: 'row',
                justifyContent: 'space-between',
                width: 'fit-content'
              }}
            >
              <div
                style={{
                  maxWidth: 500,
                  width: 'fit-content',
                  fontSize: 16,
                  paddingLeft: 8,
                  margin: 'auto',
                  whiteSpace: 'pre-wrap',
                  overflowWrap: 'break-word',
                  fontFamily: theme.typography.fontFamily
                }}
              >
                {what}
              </div>
              <div
                style={{
                  fontSize: 9,
                  color: color,
                  marginLeft: 8,
                  alignSelf: 'flex-end'
                }}
              >
                {when ? intl.formatTime(moment(when, ['hh:mm', 'hh.mm', 'DD/MM/YY hh:mm','DD/MM/YY hh.mm'])) : ''}
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

Comment.propTypes = {
  theme: PropTypes.object.isRequired
}

export default compose(
  injectIntl,
  withTheme
)(Comment)
