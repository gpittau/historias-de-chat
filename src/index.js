import * as serviceWorker from 'rmw-shell/lib/utils/serviceWorker'
import React from 'react'
import ReactDOM from 'react-dom'
import Loadable from 'react-loadable'
import Loading from './components/Loading/Loading'
import App from './App'

export const AppAsync = Loadable({
  loader: () => import('./App'),
  loading: () => Loading
})

ReactDOM.render(<App />, document.getElementById('root'))

serviceWorker.register({})
