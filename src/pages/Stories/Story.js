import EditActivity from 'rmw-shell/lib/containers/Activities/EditActivity'
import Form from '../../components/Forms/Story'
import React from 'react'
import { injectIntl } from 'react-intl'

const name = 'story'
const path = 'stories'

const Edit = props => {
  const { intl } = props
  const validate = values => {
    const errors = {}

    errors.title = !values.title ? intl.formatMessage({ id: 'error_required_field' }) : ''
    errors.author = !values.author ? intl.formatMessage({ id: 'error_required_field' }) : ''
    //errors.vat = !values.vat ? intl.formatMessage({ id: 'error_required_field' }) : ''

    return errors
  }

  return (
    <EditActivity
      name={name}
      path={path}
      fireFormProps={{
        validate
      }}
    >
      <Form {...props} />
    </EditActivity>
  )
}

export default injectIntl(Edit)

