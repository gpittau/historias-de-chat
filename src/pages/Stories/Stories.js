import AltIconAvatar from 'rmw-shell/lib/components/AltIconAvatar'
import Chat from '@material-ui/icons/Chat'
import Divider from '@material-ui/core/Divider'
import ListActivity from 'rmw-shell/lib/containers/Activities/ListActivity'
import ListItem from '@material-ui/core/ListItem'
import ListItemText from '@material-ui/core/ListItemText'
import React, { Component } from 'react'
//import { connect } from 'react-redux'
//import { setPersistentValue } from 'rmw-shell/lib/store/persistentValues/actions'
import { compose } from 'redux'
import { injectIntl } from 'react-intl'
import { withRouter } from 'react-router-dom'
import { withTheme } from '@material-ui/core/styles'

class Stories extends Component {
  renderItem = (key, val) => {
    const {
      history,
      //setPersistentValue,
      action='edit'
    } = this.props

    const { title = '', author = '' } = val

    const handleClick = () => {
      //setPersistentValue('current_story_key', key)
      history.push(`/stories/${action}/${key}`)
    }

    return (
      <div key={key}>
        <ListItem onClick={ handleClick } key={key}>
          <AltIconAvatar alt="story" src={val.photoURL} icon={<Chat />} />
          <ListItemText
            primary={title}
            secondary={author}
            style={{ minWidth: 120 }} />
        </ListItem>
        <Divider variant="inset" />
      </div>
    )
  }

  render() {
    const filterFields = [
      { name: 'title' },
      { name: 'author' }
    ]

    return (
      <ListActivity
        name="stories"
        createGrant="create_story"
        filterFields={filterFields}
        renderItem={this.renderItem}
      />
    )
  }
}

export default compose(
  injectIntl,
  withRouter,
  withTheme
)(Stories)

