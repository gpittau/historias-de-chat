import { Activity } from 'rmw-shell'
import Messages from '../../containers/Messages'
import React, { useEffect, useState } from 'react'
import { compose } from 'redux'
import { connect } from 'react-redux'
import { injectIntl } from 'react-intl'
import { withFirebase } from 'firekit-provider'
import { withTheme } from '@material-ui/core/styles'
import Button  from '@material-ui/core/Button'

const PlayStory = props => {
  const { theme, intl, uid, firebaseApp } = props

  const [currentStory, setCurrentStory] = useState({
    title: intl.formatMessage({ id: 'story' }),
    author: '',
    conversation: ''
  })

  const [currentMessage, setCurrentMessage] = useState(1)
  const handleNext = () => setCurrentMessage(currentMessage + 1)

  useEffect(() => {
    firebaseApp
      .database()
      .ref(`/stories/${uid}`)
      .on('value', snap => {
        setCurrentStory(snap.val())
      })
  },[uid, firebaseApp])

  const { title, conversation } = currentStory

  return (
    <Activity title={title} >
      <div
        style={{
          height: '100%',
          width: '100%',
          alignItems: 'strech',
          display: 'flex',
          justifyContent: 'flex-start',
          flexDirection: 'row'
        }}
      >
        <div style={{ width: '100%', display: 'flex', flexDirection: 'column', marginLeft: 0, flexGrow: 1 }}>
          <Messages currentMessage={currentMessage} conversation={conversation} { ...props } />

          <div
            style={{
              display: 'block',
              alignItems: 'row',
              justifyContent: 'center',
              backgroundColor: theme.palette.background.main,
              marginTop: 20,
              marginRight: 15,
              marginLeft: 15
            }}
          >
            <div
              style={{
                display: 'flex',
                alignItems: 'center',
                justifyContent: 'center',
              }}
            >
              <Button
                color="primary"
                variant="contained"
                onClick={handleNext}
                style={{
                  width: '100%'
                }}
              >
                Toca para continuar &gt;&gt;
              </Button>
            </div>
          </div>
        </div>
      </div>
    </Activity>
  )
}

const mapStateToProps = (state , props) => {
  const {
    match: { params: {uid} }
  } = props
  return { uid }
}
export default connect(mapStateToProps)(
  compose(
    injectIntl,
    withFirebase,
    withTheme
  )(PlayStory)
)

