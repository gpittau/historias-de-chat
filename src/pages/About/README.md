# Historias de Chat

H.D.C. (Historias De Chat) es una colección de historias fantásticas escritas en formato de conversación digital por los alumnos de quinto año A de la Escuela Secundaria N° 77, Mar del Plata, Argentina.

El sitio se ha originado a partir de la lectura de historias pertenecientes a este nuevo género literario de soporte digital durante la clase, haciendo foco en el tema Literatura fantástica, que forma parte del diseño curricular oficial. Los alumnos escribieron sus historias, las corrigieron y digitalizaron y dieron nombre a la antología que aquí se presenta al lector.

Como directora de este proyecto educativo, seguiré actualizando la plataforma H.D.C. con nuevas historias fantásticas de mis alumnos de quinto año de la Escuela 77, los actuales y los por venir.

Mar del Plata, 28 de septiembre de 2019

Edición: Virginia Katzen, profe de Literatura. Contacto: virginia.katzen@mail.ru

Programación: Gabriel Pittau. Contacto: gpittau@gmail.com

