import React, { useEffect, useState } from 'react'
import IconButton from '@material-ui/core/IconButton'
import { injectIntl } from 'react-intl'
import { Activity } from 'rmw-shell'
import ReactMarkdown from 'react-markdown'
import Scrollbar from 'rmw-shell/lib/components/Scrollbar/Scrollbar'
import README from './README.md'

require('github-markdown-css')

const About = props => {
  const [text, setText] = useState('')
  useEffect(() => {
    fetch(README)
      .then(response => response.text())
      .then(t => setText(t))
  }, [])
  const { intl } = props
  return (
    <Activity
      appBarContent={
        <IconButton
          href="https://gitlab.com/gpittau/historias-de-chat"
          target="_blank"
          rel="noopener">
        </IconButton>
      }
      title={intl.formatMessage({ id: 'about_hdc' })}
    >
      <Scrollbar>
        <div style={{ backgroundColor: 'white', padding: 12 }}>
          <ReactMarkdown className="markdown-body" source={text} />
        </div>
      </Scrollbar>
    </Activity>
  )
}

export default injectIntl(About)
