import AppBar from '@material-ui/core/AppBar'
import IconButton from '@material-ui/core/IconButton'
import LockIcon from '@material-ui/icons/Lock'
import React, { useEffect } from 'react'
import Toolbar from '@material-ui/core/Toolbar'
import Tooltip from '@material-ui/core/Tooltip'
import { Helmet } from 'react-helmet'
import { withRouter } from 'react-router-dom'
import { withStyles } from '@material-ui/core/styles'

const styles = theme => ({
  main: {
    display: 'flex',
    flexDirection: 'column',
    height: '100%'
  },
  root: {
    flexGrow: 1,
    flex: '1 0 100%'
  },
  hero: {
    display: 'flex',
    height: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#880016'
  },
  portada: {
    width: '100%',
    height: '100vh',
    cursor: 'pointer'
  }
})

const LandingPage = ({ classes, history, theme, intl}) => {
  const isAuthorised = () => {
    try {
      const key = Object.keys(localStorage).find(e => e.match(/persist:root/))
      const data = JSON.parse(localStorage.getItem(key))
      const auth = JSON.parse(data.auth)

      return auth && auth.isAuthorised
    } catch (ex) {
      return false
    }
  }

  useEffect(() => {
    if (isAuthorised()) {
      history.push('/signin')
    }
  })

  return (
    <div className={classes.main}>
      <Helmet>
        <meta name="theme-color" content={theme.palette.primary.main} />
        <meta name="apple-mobile-web-app-status-bar-style" content={theme.palette.primary.main} />
        <meta name="msapplication-navbutton-color" content={theme.palette.primary.main} />
        <title>{
          //intl.formatMessage({ id: 'app_name' })
          'HdC'
        }</title>
      </Helmet>
      <AppBar position="static">
        <Toolbar disableGutters>
          <div style={{ flex: 1 }} />

          <Tooltip id="tooltip-icon1" title="Ingresar">
            <IconButton
              name="signin"
              aria-label="Ingresar"
              color="inherit"
              onClick={() => {
                history.push('/signin')
              }}
              rel="noopener"
            >
              <LockIcon />
            </IconButton>
          </Tooltip>
        </Toolbar>
      </AppBar>

      <div className={classes.root}>
        <div className={classes.hero}>
          <img
            onClick={() => history.push('/stories/public')}
            src="/portada_full.jpg"
            alt="HdC"
            className={classes.portada} />
        </div>
      </div>
    </div>
  )
}

export default withRouter(withStyles(styles, { withTheme: true })(LandingPage))
//export default withRouter(withStyles(styles, { withTheme: true })(injectIntl(LandingPage)))
//export default compose(
//  injectIntl,
//  withRouter,
//  withStyles(styles, {withTheme: true})
//)(LandingPage)
