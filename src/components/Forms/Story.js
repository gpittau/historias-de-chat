import PropTypes from 'prop-types'
import React, { Component } from 'react'
import TextField from 'rmw-shell/lib/components/ReduxFormFields/TextField'
import { Field, reduxForm } from 'redux-form'
import { connect } from 'react-redux'
import { injectIntl } from 'react-intl'
import { setDialogIsOpen } from 'rmw-shell/lib/store/dialogs/actions'
import { withRouter } from 'react-router-dom'
import { withTheme } from '@material-ui/core/styles'

class FormClass extends Component {
  render() {
    const { handleSubmit, intl, initialized } = this.props

    return (
      <form
        onSubmit={handleSubmit}
        style={{
          height: '100%',
          width: '100%',
          alignItems: 'strech',
          display: 'flex',
          flexWrap: 'wrap',
          justifyContent: 'center'
        }}
      >
        <button type="submit" style={{ display: 'none' }} />

        <div style={{ margin: 15, display: 'flex', flexDirection: 'column' }}>

          <div>
            <Field
              name="title"
              disabled={!initialized}
              component={TextField}
              placeholder={intl.formatMessage({ id: 'title_hint' })}
              label={intl.formatMessage({ id: 'title_label' })}
            />
          </div>

          <div>
            <Field
              name="author"
              disabled={!initialized}
              component={TextField}
              placeholder={intl.formatMessage({ id: 'author_hint' })}
              label={intl.formatMessage({ id: 'author_label' })}
            />
          </div>

          <div>
            <Field
              name="conversation"
              disabled={!initialized}
              component={TextField}
              multiline
              rows={8}
              placeholder={intl.formatMessage({ id: 'conversation_hint' })}
              label={intl.formatMessage({ id: 'conversation_label' })}
            />
          </div>

        </div>
      </form>
    )
  }
}

FormClass.propTypes = {
  handleSubmit: PropTypes.func.isRequired,
  initialized: PropTypes.bool.isRequired,
  setDialogIsOpen: PropTypes.func.isRequired,
  dialogs: PropTypes.object.isRequired,
  match: PropTypes.object.isRequired
}

let Form = reduxForm({ form: 'story' })(FormClass)
//const selector = formValueSelector('story')

const mapStateToProps = state => {
  const { intl, users, dialogs } = state

  return {
    intl,
    users,
    dialogs
  }
}

export default connect(
  mapStateToProps,
  { setDialogIsOpen }
)(injectIntl(withRouter(withTheme(Form))))
