import React from 'react'
import LoadingComponent from 'rmw-shell/lib/components/LoadingComponent'

const Loading = props => {
  if(props.isLoading && props.pastDelay) {
    return (
      <div className={props.classes.container}>
        <img src="/hdc.sgv" className={props.classes.loader} alt="logo" />
      </div>
    )
  } else {
    return <LoadingComponent />
  }
}

export default Loading
