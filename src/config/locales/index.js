import en_messages from './en'
import es_messages from './es'
import '@formatjs/intl-relativetimeformat/polyfill'
import '@formatjs/intl-relativetimeformat/dist/locale-data/en'
import '@formatjs/intl-relativetimeformat/dist/locale-data/es'

const locales = [
  {
    locale: 'en',
    messages: en_messages
  },
  {
    locale: 'es',
    messages: es_messages
  }
]

export default locales
