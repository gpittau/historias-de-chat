import React from 'react'
import Loadable from 'react-loadable'
import getMenuItems from './menuItems'
import LoadingComponent from 'rmw-shell/lib/components/LoadingComponent'
import locales from './locales'
import routes from './routes'
import themes from './themes'
import grants from './grants'
import HdCIcon from '../components/Icons/HdCIcon'

import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles'
const [{source: defaultTheme}] = themes
const theme = createMuiTheme(defaultTheme)
const Loading = () => <LoadingComponent />
const LPAsync = Loadable({
  loader: () => import('../../src/pages/LandingPage'),
  render: ({default: Component}, props) => (
    <MuiThemeProvider theme={theme}>
      <Component {...props} />
    </MuiThemeProvider>),
  loading: Loading
})

const config = {
  appIcon: HdCIcon,
  firebase_config: {
    apiKey: 'AIzaSyD-nYU7Bz5kiU6HAXNo2pBZWeqvXdthdUE',
    authDomain: 'historiasdechat.firebaseapp.com',
    databaseURL: 'https://historiasdechat.firebaseio.com',
    projectId: 'historiasdechat',
    storageBucket: 'historiasdechat.appspot.com',
    messagingSenderId: '410004454355',
    appId: '1:410004454355:web:7c4d4f0b8172ac7dd40d26',
    measurementId: 'G-VGW757RD48'
  },
  firebase_config_dev: {
    apiKey: 'AIzaSyCYc4pcWy6ELItwzrtfaWfV7UZWDLsYLEc',
    authDomain: 'hdc-dev-6957e.firebaseapp.com',
    databaseURL: 'https://hdc-dev-6957e.firebaseio.com',
    projectId: 'hdc-dev-6957e',
    storageBucket: '',
    messagingSenderId: '169647024810',
    appId: '1:169647024810:web:e35d0193bb6fd7e954bed3'
  },
  firebase_providers: ['google.com'],
  initial_state: {
    themeSource: {
      isNightModeOn: true,
      source: 'peach'
    },
    locale: 'es'
  },
  drawer_width: 256,
  locales,
  themes,
  grants,
  routes,
  getMenuItems,
  firebaseLoad: () => import('./firebase'),
  landingPage: LPAsync
}

export default config
