/* eslint-disable react/jsx-key */
import React from 'react'
import { Route } from 'react-router'
import RestrictedRoute from 'rmw-shell/lib/containers/RestrictedRoute'
import makeLoadable from 'rmw-shell/lib/containers/MyLoadable'

const MyLoadable = (opts, preloadComponents) =>
  makeLoadable({ ...opts, firebase: () => import('./firebase') }, preloadComponents)

const AsyncAbout = MyLoadable({ loader: () => import('../pages/About') })

const AsyncStory = MyLoadable({ loader: () => import('../pages/Stories/Story') })
const AsyncPlayStory = MyLoadable({ loader: () => import('../pages/Stories/PlayStory') })
const AsyncStories = MyLoadable({ loader: () => import('../pages/Stories/Stories') }, [AsyncStory])
const AsyncViewStories = () => <AsyncStories action="view" />


const routes = [
  <Route path="/stories/public" exact component={AsyncViewStories} />,
  <Route path="/stories/view/:uid" exact component={AsyncPlayStory} />,
  <Route path="/about" exact component={AsyncAbout} />,
  <Route path="/" exact component={AsyncViewStories} />,

  <RestrictedRoute type="private" path="/stories" exact component={AsyncStories} />,
  <RestrictedRoute type="private" path="/stories/edit/:uid" exact component={AsyncStory} />,
  <RestrictedRoute type="private" path="/stories/create" exact component={AsyncStory} />

]

export default routes
