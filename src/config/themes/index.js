import red from '@material-ui/core/colors/red'

const themes = [
  {
    id: 'peach',
    source: {
      palette: {
        primary: { main: '#FFCC80' },
        secondary: { main: '#C5E1A5' },
        error: red
      }
    }
  },
  {
    id: 'sunset',
    source: {
      palette: {
        primary: { main: '#D1C4E9' },
        secondary: { main: '#baddf9' },
        error: red
      }
    }
  },
  {
    id: 'cookie',
    source: {
      palette: {
        primary: { main: '#fff59d' },
        secondary: { main: '#FFCDD2' },
        error: red
      }
    }
  }
]

export default themes
