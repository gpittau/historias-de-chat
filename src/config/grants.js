export const grants = [
  'create_story',
  'edit_story',
  'delete_story',
  'read_stories',
]

export default grants
